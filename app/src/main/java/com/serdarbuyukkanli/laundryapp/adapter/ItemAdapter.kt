package com.serdarbuyukkanli.laundryapp.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.RecyclerView
import com.serdarbuyukkanli.laundryapp.LaundryApp
import com.serdarbuyukkanli.laundryapp.R
import com.serdarbuyukkanli.laundryapp.activity.BaseActivity
import com.serdarbuyukkanli.laundryapp.model.Item
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.cell_item.view.*
import java.text.DecimalFormat

class ItemAdapter(
    val context: Context,
    val items: ArrayList<Item>
) :
    RecyclerView.Adapter<ItemAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val item = items[position]
        holder.tvTitle?.text = item.title
        Picasso.get().load(item.iconx2).into(holder.ivItemImage)
        handleServices(holder, item)

        var favouriteItemIds =
            ((context as BaseActivity).application as LaundryApp).favouriteItemIds

        var markedAsFavourite = false
        for (i in 0 until favouriteItemIds.size) {
            if (favouriteItemIds[i] == item.id) {
                markedAsFavourite = true
            }
        }
        if (markedAsFavourite) {
            holder.ivFavourite?.setImageDrawable(context.resources.getDrawable(R.drawable.ic_favourite_filled))
        } else {
            holder.ivFavourite?.setImageDrawable(context.resources.getDrawable(R.drawable.ic_favourite))
        }

        holder.tvAddToBasket?.setOnClickListener {
            val intent = Intent("onBasketStatusChanged")
            intent.putExtra("price", calculatePrice(item))
            intent.putExtra("itemId", item.id)
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent)

            item.washDryChecked = false
            item.ironChecked = false
            item.hangChecked = false
            item.foldChecked = false
            item.dryCleanChecked = false
            notifyDataSetChanged()
        }

        holder.ivFavourite?.setOnClickListener {
            val intent = Intent("onFavouriteStatusChanged")
            intent.putExtra("itemId", item.id)
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
            notifyDataSetChanged()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.cell_item, parent, false)
        return ViewHolder(v)

    }

    private fun handleServices(holder: ViewHolder, item: Item) {

        holder.cbWashAndDry?.let { handleCheckBox(it, item.washDry == "1", item.washDryChecked) }
        holder.cbIron?.let { handleCheckBox(it, item.iron == "1", item.ironChecked) }
        holder.cbHang?.let { handleCheckBox(it, item.hang == "1", item.hangChecked) }
        holder.cbFold?.let { handleCheckBox(it, item.fold == "1", item.foldChecked) }
        holder.cbDryClean?.let { handleCheckBox(it, item.dryClean == "1", item.dryCleanChecked) }

        holder.cbWashAndDry?.setOnCheckedChangeListener { _, isChecked ->
            item.washDryChecked = isChecked
            notifyDataSetChanged()
        }
        holder.cbIron?.setOnCheckedChangeListener { _, isChecked ->
            item.ironChecked = isChecked
            notifyDataSetChanged()
        }
        holder.cbHang?.setOnCheckedChangeListener { _, isChecked ->
            item.hangChecked = isChecked
            notifyDataSetChanged()
        }
        holder.cbFold?.setOnCheckedChangeListener { _, isChecked ->
            item.foldChecked = isChecked
            notifyDataSetChanged()
        }
        holder.cbDryClean?.setOnCheckedChangeListener { _, isChecked ->
            item.dryCleanChecked = isChecked
            notifyDataSetChanged()
        }

        val df = DecimalFormat("#.##")
        holder.tvPrice?.text = "$" + df.format(calculatePrice(item))

    }

    private fun calculatePrice(item: Item): Double {
        var price = 0.0
        if (item.washDryChecked) {
            price += item.washDryPrice.toDouble()
        }
        if (item.ironChecked) {
            price += item.ironPrice.toDouble()
        }
        if (item.hangChecked) {
            price += item.hangPrice.toDouble()
        }
        if (item.foldChecked) {
            price += item.foldPrice.toDouble()
        }
        if (item.dryCleanChecked) {
            price += item.dryCleanPrice.toDouble()
        }

        return price
    }

    private fun handleCheckBox(checkBox: CheckBox, enabled: Boolean, checked: Boolean) {
        checkBox.setOnCheckedChangeListener(null)
        checkBox.isEnabled = enabled
        checkBox.isChecked = checked
    }

    override fun getItemCount(): Int {
        return items.size
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvTitle: TextView? = view.tv_title
        val ivItemImage: ImageView? = view.iv_item_image
        val tvPrice: TextView? = view.tv_price
        val ivFavourite: ImageView? = view.iv_favourite
        val tvAddToBasket: TextView? = view.tv_add_to_basket
        val cbWashAndDry: CheckBox? = view.cb_wash_and_dry
        val cbIron: CheckBox? = view.cb_iron
        val cbHang: CheckBox? = view.cb_hang
        val cbFold: CheckBox? = view.cb_fold
        val cbDryClean: CheckBox? = view.cb_dry_clean

    }

}