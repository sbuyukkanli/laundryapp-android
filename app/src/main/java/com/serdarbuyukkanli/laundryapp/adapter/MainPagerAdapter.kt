package com.serdarbuyukkanli.laundryapp.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.serdarbuyukkanli.laundryapp.fragment.EmptyFragment
import com.serdarbuyukkanli.laundryapp.fragment.HomeFragment

class MainPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(
    fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
) {
    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> {
                return HomeFragment()
            }
        }
        return EmptyFragment()
    }

    override fun getCount(): Int {
        return 4
    }

}