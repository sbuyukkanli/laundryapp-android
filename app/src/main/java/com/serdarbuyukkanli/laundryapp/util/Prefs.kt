package com.serdarbuyukkanli.laundryapp.util

import android.content.Context
import android.content.SharedPreferences

class Prefs private constructor(context: Context) {

    private val sharedPreferences: SharedPreferences? =
        context.getSharedPreferences("Prefs", Context.MODE_PRIVATE)

    fun saveString(key: String, value: String) {
        val prefsEditor = sharedPreferences!!.edit()
        prefsEditor.putString(key, value)
        prefsEditor.commit()
    }

    fun getString(key: String): String {
        return sharedPreferences?.getString(key, "") ?: ""
    }

    fun saveBoolean(key: String, value: Boolean) {
        val prefsEditor = sharedPreferences!!.edit()
        prefsEditor.putBoolean(key, value)
        prefsEditor.commit()
    }

    fun getBoolean(key: String, defValue: Boolean): Boolean {
        return sharedPreferences?.getBoolean(key, defValue) ?: defValue
    }

    fun saveFloat(key: String, value: Float) {
        val prefsEditor = sharedPreferences!!.edit()
        prefsEditor.putFloat(key, value)
        prefsEditor.commit()
    }

    fun getFloat(key: String): Float {
        return sharedPreferences?.getFloat(key, 0f) ?: 0f
    }

    companion object {
        private var yourPreference: Prefs? = null
        fun getInstance(context: Context): Prefs {
            if (yourPreference == null) {
                yourPreference = Prefs(context)
            }
            return yourPreference as Prefs
        }
    }

}