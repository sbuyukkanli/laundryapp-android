package com.serdarbuyukkanli.laundryapp.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class CategoryWrapper() : Parcelable {
    @SerializedName("data")
    lateinit var data: List<Category>

    constructor(parcel: Parcel) : this() {
        data = parcel.createTypedArrayList(Category)!!
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(data)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CategoryWrapper> {
        override fun createFromParcel(parcel: Parcel): CategoryWrapper {
            return CategoryWrapper(parcel)
        }

        override fun newArray(size: Int): Array<CategoryWrapper?> {
            return arrayOfNulls(size)
        }
    }
}