package com.serdarbuyukkanli.laundryapp.model

import com.google.gson.annotations.SerializedName

class ItemWrapper {
    @SerializedName("data")
    lateinit var data: List<Item>
}