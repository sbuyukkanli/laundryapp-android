package com.serdarbuyukkanli.laundryapp.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class Item() : Parcelable{
    @SerializedName("id")
    lateinit var id: String
    @SerializedName("cat_id")
    lateinit var catId: String
    @SerializedName("title")
    lateinit var title: String
    @SerializedName("iconx2")
    lateinit var iconx2: String

    @SerializedName("wash_dry")
    lateinit var washDry: String
    @SerializedName("wash_dry_price")
    lateinit var washDryPrice: String
    var washDryChecked: Boolean = false

    @SerializedName("fold")
    lateinit var fold: String
    @SerializedName("fold_price")
    lateinit var foldPrice: String
    var foldChecked: Boolean = false

    @SerializedName("iron")
    lateinit var iron: String
    @SerializedName("iron_price")
    lateinit var ironPrice: String
    var ironChecked: Boolean = false

    @SerializedName("hang")
    lateinit var hang: String
    @SerializedName("hang_price")
    lateinit var hangPrice: String
    var hangChecked: Boolean = false

    @SerializedName("dry_clean")
    lateinit var dryClean: String
    @SerializedName("dry_clean_price")
    lateinit var dryCleanPrice: String

    var dryCleanChecked: Boolean = false

    constructor(parcel: Parcel) : this() {
        id = parcel.readString().toString()
        catId = parcel.readString().toString()
        title = parcel.readString().toString()
        iconx2 = parcel.readString().toString()
        washDry = parcel.readString().toString()
        washDryPrice = parcel.readString().toString()
        washDryChecked = parcel.readByte() != 0.toByte()
        fold = parcel.readString().toString()
        foldPrice = parcel.readString().toString()
        foldChecked = parcel.readByte() != 0.toByte()
        iron = parcel.readString().toString()
        ironPrice = parcel.readString().toString()
        ironChecked = parcel.readByte() != 0.toByte()
        hang = parcel.readString().toString()
        hangPrice = parcel.readString().toString()
        hangChecked = parcel.readByte() != 0.toByte()
        dryClean = parcel.readString().toString()
        dryCleanPrice = parcel.readString().toString()
        dryCleanChecked = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(catId)
        parcel.writeString(title)
        parcel.writeString(iconx2)
        parcel.writeString(washDry)
        parcel.writeString(washDryPrice)
        parcel.writeByte(if (washDryChecked) 1 else 0)
        parcel.writeString(fold)
        parcel.writeString(foldPrice)
        parcel.writeByte(if (foldChecked) 1 else 0)
        parcel.writeString(iron)
        parcel.writeString(ironPrice)
        parcel.writeByte(if (ironChecked) 1 else 0)
        parcel.writeString(hang)
        parcel.writeString(hangPrice)
        parcel.writeByte(if (hangChecked) 1 else 0)
        parcel.writeString(dryClean)
        parcel.writeString(dryCleanPrice)
        parcel.writeByte(if (dryCleanChecked) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Item> {
        override fun createFromParcel(parcel: Parcel): Item {
            return Item(parcel)
        }

        override fun newArray(size: Int): Array<Item?> {
            return arrayOfNulls(size)
        }
    }

}