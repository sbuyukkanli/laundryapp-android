package com.serdarbuyukkanli.laundryapp.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class Category() : Parcelable{
    @SerializedName("id")
    lateinit var id: String
    @SerializedName("order_no")
    lateinit var orderNo: String
    @SerializedName("cat_title")
    lateinit var catTitle: String
    var items = ArrayList<Item>()

    constructor(parcel: Parcel) : this() {
        id = parcel.readString().toString()
        orderNo = parcel.readString().toString()
        catTitle = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(orderNo)
        parcel.writeString(catTitle)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Category> {
        override fun createFromParcel(parcel: Parcel): Category {
            return Category(parcel)
        }

        override fun newArray(size: Int): Array<Category?> {
            return arrayOfNulls(size)
        }
    }
}