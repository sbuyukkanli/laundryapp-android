package com.serdarbuyukkanli.laundryapp

import android.app.Application
import com.google.gson.Gson
import com.serdarbuyukkanli.laundryapp.model.ItemIdWrapper
import com.serdarbuyukkanli.laundryapp.util.Prefs
import kotlin.collections.ArrayList

class LaundryApp : Application() {

    var basketPrice: Double = 0.0
    var username: String = ""
    var profilePicturePath: String = ""
    var favouriteItemIds = ArrayList<String>()
    var language: String = "en"
    var basketItemIds = ArrayList<String>()

    override fun onCreate() {
        super.onCreate()
        fetchPrefs()
    }

    private fun fetchPrefs() {
        basketPrice =
            Prefs.getInstance(context = this).getFloat("basketPrice").toDouble()
        username =
            Prefs.getInstance(context = this).getString("username")

        if (Prefs.getInstance(context = this).getString("language").isNotEmpty()) {
            language =
                Prefs.getInstance(context = this).getString("language")
        }

        if (Prefs.getInstance(context = this).getString("profilePicturePath").isNotEmpty()) {
            profilePicturePath =
                Prefs.getInstance(context = this).getString("profilePicturePath")
        }

        val gson = Gson()
        val jsonFavouriteItemIds = Prefs.getInstance(context = this).getString("favouriteWrapper")
        if (jsonFavouriteItemIds.isNotEmpty()) {
            favouriteItemIds =
                gson.fromJson(jsonFavouriteItemIds, ItemIdWrapper::class.java).itemIds
        }

        val jsonBasketItemIds = Prefs.getInstance(context = this).getString("basketWrapper")
        if (jsonBasketItemIds.isNotEmpty()) {
            basketItemIds =
                gson.fromJson(jsonBasketItemIds, ItemIdWrapper::class.java).itemIds
        }
    }

    fun getAppLanguageForRequest(): String {
        return if (language == "es") {
            "2"
        } else {
            "1"
        }
    }
}