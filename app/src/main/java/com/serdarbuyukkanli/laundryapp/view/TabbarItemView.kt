package com.serdarbuyukkanli.laundryapp.view

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.serdarbuyukkanli.laundryapp.R
import kotlinx.android.synthetic.main.view_tabbar_item.view.*

class TabbarItemView(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {

    var tvName: TextView? = null
    var ivIcon: ImageView? = null
    private var styledAttrs: TypedArray = context.obtainStyledAttributes(attrs, R.styleable.TabbarItemView)

    init {
        val view = LayoutInflater.from(context).inflate(R.layout.view_tabbar_item, this)
        tvName = view.tv_name
        ivIcon = view.iv_icon
        tvName?.text = styledAttrs.getString(R.styleable.TabbarItemView_name)
        ivIcon?.setImageResource(styledAttrs.getResourceId(R.styleable.TabbarItemView_image, 0))
        isSelected = styledAttrs.getBoolean(R.styleable.TabbarItemView_selected, false)
        select(isSelected)
    }

    fun select(selected: Boolean) {
        if (selected) {
            ivIcon?.setImageResource(
                styledAttrs.getResourceId(
                    R.styleable.TabbarItemView_image_active,
                    0
                )
            )
            ivIcon?.alpha = 1f
            tvName?.typeface = Typeface.DEFAULT_BOLD
            tvName?.alpha = 1f

        } else {
            ivIcon?.setImageResource(styledAttrs.getResourceId(R.styleable.TabbarItemView_image, 0))
            ivIcon?.alpha = 0.7f
            tvName?.typeface = Typeface.DEFAULT
            tvName?.alpha = 0.7f
        }
    }
}