package com.serdarbuyukkanli.laundryapp.fragment

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.StrictMode
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.gson.Gson
import com.mvc.imagepicker.ImagePicker
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems
import com.serdarbuyukkanli.laundryapp.LaundryApp
import com.serdarbuyukkanli.laundryapp.R
import com.serdarbuyukkanli.laundryapp.activity.BaseActivity
import com.serdarbuyukkanli.laundryapp.model.CategoryWrapper
import com.serdarbuyukkanli.laundryapp.model.ItemIdWrapper
import com.serdarbuyukkanli.laundryapp.model.ItemWrapper
import com.serdarbuyukkanli.laundryapp.network.RestApi
import com.serdarbuyukkanli.laundryapp.util.Prefs
import kotlinx.android.synthetic.main.fragment_home.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.FileNotFoundException
import java.text.DecimalFormat

class HomeFragment : Fragment() {

    lateinit var service: RestApi
    lateinit var categoryWrapper: CategoryWrapper
    var basketPrice: Double = 0.0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        registerBroadcastManagers()
        basketPrice = (activity?.application as LaundryApp).basketPrice
        service = (activity as BaseActivity).service
        handleOnClick()
        showNameInputDialogIfNecessary(false)
        showProfilePictureIfExists()
        getCategories()
    }

    private fun registerBroadcastManagers() {
        context?.let {
            LocalBroadcastManager.getInstance(it).registerReceiver(
                onBasketStatusChanged,
                IntentFilter("onBasketStatusChanged")
            )
        }
        context?.let {
            LocalBroadcastManager.getInstance(it).registerReceiver(
                onFavouriteStatusChanged,
                IntentFilter("onFavouriteStatusChanged")
            )
        }
        context?.let {
            LocalBroadcastManager.getInstance(it).registerReceiver(
                onUsernameChanged,
                IntentFilter("onUsernameChanged")
            )
        }
        context?.let {
            LocalBroadcastManager.getInstance(it).registerReceiver(
                onProfilePictureSelected,
                IntentFilter("onProfilePictureSelected")
            )
        }
    }


    private fun setupViewPagerTabs(categoryWrapper: CategoryWrapper) {

        val pages = FragmentPagerItems.Creator(context)

        for (i in categoryWrapper.data.indices) {
            val bundle = Bundle()
            bundle.putParcelable("category", categoryWrapper.data[i])
            pages.add(categoryWrapper.data[i].catTitle, CategoryFragment::class.java, bundle)
        }

        val adapter = FragmentPagerItemAdapter(childFragmentManager, pages.create())

        vp_tabs.adapter = adapter
        stl_tabs.setViewPager(vp_tabs)
    }

    private fun getCategories() {
        val language = (activity?.application as LaundryApp).getAppLanguageForRequest()
        service.getCategories(language).enqueue(object : Callback<CategoryWrapper> {
            override fun onFailure(call: Call<CategoryWrapper>, t: Throwable) {
            }

            override fun onResponse(
                call: Call<CategoryWrapper>,
                response: Response<CategoryWrapper>
            ) {
                if (response.isSuccessful) {
                    categoryWrapper = response.body()!!
                    getItems(categoryWrapper)
                }
            }

        })
    }

    private fun getItems(categoryWrapper: CategoryWrapper) {
        val language = (activity?.application as LaundryApp).getAppLanguageForRequest()
        service.getItems(language).enqueue(object : Callback<ItemWrapper> {
            override fun onFailure(call: Call<ItemWrapper>, t: Throwable) {
            }

            override fun onResponse(
                call: Call<ItemWrapper>,
                response: Response<ItemWrapper>
            ) {
                if (response.isSuccessful) {

                    response.body()?.let { putItemsToRelatedCategories(categoryWrapper, it) }
                    checkBasket()
                    setupViewPagerTabs(categoryWrapper)
                }
            }

        })
    }

    private fun putItemsToRelatedCategories(
        categoryWrapper: CategoryWrapper,
        itemWrapper: ItemWrapper
    ) {
        for (i in itemWrapper.data.indices) {
            for (j in categoryWrapper.data.indices) {
                if (itemWrapper.data[i].catId == categoryWrapper.data[j].id) {
                    categoryWrapper.data[j].items.add(itemWrapper.data[i])
                }
            }
        }
    }

    private val onBasketStatusChanged = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val price = intent.extras?.getDouble("price")
            val itemId = intent.extras?.getString("itemId")

            if (price!! > 0) {
                basketPrice += price.toDouble()
                (activity?.application as LaundryApp).basketPrice = basketPrice
                activity?.applicationContext?.let {
                    Prefs.getInstance(context = it)
                        .saveFloat("basketPrice", basketPrice.toFloat())
                }

                itemId?.let { (activity?.application as LaundryApp).basketItemIds.add(it) }
                val itemIdWrapper = ItemIdWrapper()
                itemIdWrapper.itemIds = (activity?.application as LaundryApp).basketItemIds

                val gson = Gson()
                val itemIdWrapperJson = gson.toJson(itemIdWrapper)
                activity?.applicationContext?.let {
                    Prefs.getInstance(context = it)
                        .saveString("basketWrapper", itemIdWrapperJson)
                }

                checkBasket()
            }

        }
    }

    private val onProfilePictureSelected = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            showProfilePictureIfExists()
        }
    }

    private val onFavouriteStatusChanged = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val itemId = intent.extras?.getString("itemId")

            val favouriteItemIds = (activity?.application as LaundryApp).favouriteItemIds
            var removed = false
            for (i in 0 until favouriteItemIds.size) {
                if (favouriteItemIds[i] == itemId) {
                    favouriteItemIds.removeAt(i)
                    removed = true
                    break
                }
            }
            if (!removed) {
                itemId?.let { favouriteItemIds.add(it) }
            }

            val itemIdWrapper = ItemIdWrapper()
            itemIdWrapper.itemIds = favouriteItemIds

            val gson = Gson()
            val itemIdWrapperJson = gson.toJson(itemIdWrapper)
            activity?.applicationContext?.let {
                Prefs.getInstance(context = it)
                    .saveString("favouriteWrapper", itemIdWrapperJson)
            }
        }
    }


    private val onUsernameChanged = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val username = intent.extras?.getString("username")
            (activity?.application as LaundryApp).username = username.toString()
            activity?.applicationContext?.let {
                username?.let { it1 ->
                    Prefs.getInstance(context = it)
                        .saveString("username", it1)
                }
            }

            var welcomeText = getString(R.string.hey_there)
            if (username!!.isNotEmpty()) {
                welcomeText += ", $username"
            }
            tv_username.text = welcomeText
        }
    }

    private fun checkBasket() {

        if (basketPrice > 0) {
            val df = DecimalFormat("#.##")
            tv_basket_price.text = "$" + df.format(basketPrice)
            rl_basket.visibility = View.VISIBLE
        } else {
            rl_basket.visibility = View.GONE
        }

        val intent = Intent("onItemAddedToBasket")
        intent.putExtra("count", (activity?.application as LaundryApp).basketItemIds.size)
        context?.let { LocalBroadcastManager.getInstance(it).sendBroadcast(intent) }
    }

    override fun onDestroy() {
        super.onDestroy()
        context?.let {
            LocalBroadcastManager.getInstance(it).unregisterReceiver(onBasketStatusChanged)
        }
        context?.let {
            LocalBroadcastManager.getInstance(it).unregisterReceiver(onFavouriteStatusChanged)
        }
        context?.let {
            LocalBroadcastManager.getInstance(it).unregisterReceiver(onUsernameChanged)
        }
        context?.let {
            LocalBroadcastManager.getInstance(it).unregisterReceiver(onProfilePictureSelected)
        }
    }


    private fun showNameInputDialogIfNecessary(withoutCheck: Boolean) {
        val username = (activity?.application as LaundryApp).username

        val nameDialogPrompted = activity?.applicationContext?.let {
            Prefs.getInstance(context = it)
                .getBoolean("nameDialogPrompted", false)
        }

        if (!nameDialogPrompted!! || withoutCheck) {
            activity?.applicationContext?.let {
                Prefs.getInstance(context = it)
                    .saveBoolean("nameDialogPrompted", true)
            }
            val fragmentManager = (activity as BaseActivity).fragmentManager
            val nameInputDialogFragment = NameInputDialogFragment.newInstance()
            nameInputDialogFragment.show(
                fragmentManager,
                "nameInputDialogFragment"
            )
        } else {
            var welcomeText = getString(R.string.hey_there)
            if (username.isNotEmpty()) {
                welcomeText += ", $username"
            }
            tv_username.text = welcomeText
        }
    }

    private fun showProfilePictureIfExists() {
        val profilePicturePath = (activity?.application as LaundryApp).profilePicturePath
        try {
            val options = BitmapFactory.Options()
            options.inPreferredConfig = Bitmap.Config.ARGB_8888
            val bitmap = BitmapFactory.decodeFile(profilePicturePath, options)
            if (bitmap != null) {
                iv_profile_picture.setImageBitmap(bitmap)
            }
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }

    }

    private fun handleOnClick() {

        val clickListener = View.OnClickListener { view ->

            when (view.id) {
                R.id.tv_username -> {
                    showNameInputDialogIfNecessary(true)
                }
                R.id.iv_profile_picture -> {

                    if (ContextCompat.checkSelfPermission(
                            activity as BaseActivity,
                            Manifest.permission.CAMERA
                        ) and PackageManager.PERMISSION_GRANTED != 0
                    ) {
                        activity?.let {
                            ActivityCompat.requestPermissions(
                                it,
                                arrayOf(Manifest.permission.CAMERA),
                                2
                            )
                        }
                    } else {
                        val builder = StrictMode.VmPolicy.Builder()
                        StrictMode.setVmPolicy(builder.build())
                        ImagePicker.pickImage(activity, getString(R.string.select_your_image))
                    }
                }
            }
        }
        tv_username.setOnClickListener(clickListener)
        iv_profile_picture.setOnClickListener(clickListener)
    }

}