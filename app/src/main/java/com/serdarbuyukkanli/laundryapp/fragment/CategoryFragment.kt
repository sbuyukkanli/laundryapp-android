package com.serdarbuyukkanli.laundryapp.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.serdarbuyukkanli.laundryapp.R
import com.serdarbuyukkanli.laundryapp.adapter.ItemAdapter
import com.serdarbuyukkanli.laundryapp.model.Category
import com.serdarbuyukkanli.laundryapp.model.Item
import kotlinx.android.synthetic.main.fragment_category.*
import kotlin.collections.ArrayList

class CategoryFragment : Fragment() {

    private lateinit var category: Category
    private var searchKey = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val bundle = this.arguments
        if (bundle != null) {
            category = bundle.getParcelable("category")!!
        }

        return inflater.inflate(R.layout.fragment_category, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handleSearchBar()
        handleRecyclerView()
    }

    private fun handleRecyclerView() {
        if (rl_items != null) {
            val linearLayoutManager = LinearLayoutManager(context)
            rl_items.layoutManager = linearLayoutManager
            val matchAdapter = activity?.let { ItemAdapter(it, filterItems()) }
            rl_items.adapter = matchAdapter
        }
    }

    private fun handleSearchBar() {
        et_search.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                searchKey = charSequence.toString()
                handleRecyclerView()
            }

            override fun afterTextChanged(editable: Editable) {
            }
        })
    }

    private fun filterItems(): ArrayList<Item> {
        return if (searchKey.isEmpty()) {
            category.items
        } else {
            val filteredItems = ArrayList<Item>()
            for (i in 0 until category.items.size) {
                if (category.items[i].title.toLowerCase().contains(searchKey.toLowerCase())) {
                    filteredItems.add(category.items[i])
                }
            }
            filteredItems
        }
    }

}