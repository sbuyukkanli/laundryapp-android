package com.serdarbuyukkanli.laundryapp.fragment

import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.EditText
import android.widget.TextView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import butterknife.ButterKnife
import butterknife.Unbinder
import com.serdarbuyukkanli.laundryapp.R

class NameInputDialogFragment : DialogFragment() {

    private var unbinder: Unbinder? = null
    var etEmail: EditText? = null
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val inflater = LayoutInflater.from(activity)
        val dialogView = inflater.inflate(R.layout.dialog_fragment_name_input, null)

        unbinder = ButterKnife.bind(this, dialogView)

        val builder = AlertDialog.Builder(activity)
        builder.setView(dialogView)

        val alertDialog = builder.create()
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog.window!!.setDimAmount(0.6f)
        alertDialog.window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)

        val tvContinue = dialogView.findViewById<TextView>(R.id.tv_continue)
        etEmail = dialogView.findViewById(R.id.et_email)

        tvContinue.setOnClickListener(clickListener)

        return alertDialog
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder!!.unbind()
    }


    private val clickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.tv_continue -> {
                val intent = Intent("onUsernameChanged")
                intent.putExtra("username", etEmail?.text.toString().trim())
                LocalBroadcastManager.getInstance(activity).sendBroadcast(intent)
                dismiss()
            }
        }
    }

    companion object {
        fun newInstance(): NameInputDialogFragment {
            val f = NameInputDialogFragment()
            val args = Bundle()
            f.arguments = args
            return f
        }
    }

}