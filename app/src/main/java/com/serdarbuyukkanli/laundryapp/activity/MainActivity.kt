package com.serdarbuyukkanli.laundryapp.activity

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.StrictMode
import android.view.View
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.mvc.imagepicker.ImagePicker
import com.serdarbuyukkanli.laundryapp.LaundryApp
import com.serdarbuyukkanli.laundryapp.R
import com.serdarbuyukkanli.laundryapp.adapter.MainPagerAdapter
import com.serdarbuyukkanli.laundryapp.util.Prefs
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.tabbar.*

class MainActivity : BaseActivity() {

    override fun getContext(): Context {
        return this
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }

    companion object {
        @JvmStatic
        fun newIntent(context: Context): Intent {
            val intent = Intent(context, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        registerBroadcastManagers()
        handleOnClick()
        handleViewPagerEvents()
        handleLanguageIcon()
    }

    private fun selectMenuItem(index: Int) {

        tiv_home.select(false)
        tiv_chats.select(false)
        tiv_orders.select(false)
        tiv_more.select(false)

        when (index) {
            0 -> {
                tiv_home.select(true)
                vp_home.currentItem = 0
                tv_title.text = ""
            }
            1 -> {
                tiv_chats.select(true)
                vp_home.currentItem = 1
                tv_title.text = getString(R.string.chats)
            }
            2 -> {
                tiv_orders.select(true)
                vp_home.currentItem = 2
                tv_title.text = getString(R.string.orders)
            }
            3 -> {
                tiv_more.select(true)
                vp_home.currentItem = 3
                tv_title.text = getString(R.string.more)
            }
        }

    }

    private fun handleOnClick() {

        val clickListener = View.OnClickListener { view ->

            when (view.id) {
                R.id.tiv_home -> {
                    selectMenuItem(0)
                }
                R.id.tiv_chats -> {
                    selectMenuItem(1)
                }
                R.id.tiv_orders -> {
                    selectMenuItem(2)
                }
                R.id.tiv_more -> {
                    selectMenuItem(3)
                }
                R.id.iv_language -> {
                    if ((application as LaundryApp).language == "en") {
                        (application as LaundryApp).language = "es"
                    } else if ((application as LaundryApp).language == "es") {
                        (application as LaundryApp).language = "en"
                    }
                    Prefs.getInstance(context = applicationContext)
                        .saveString("language", (application as LaundryApp).language)

                    startActivity(newIntent(getContext()))

                }
            }
        }

        tiv_home.setOnClickListener(clickListener)
        tiv_chats.setOnClickListener(clickListener)
        tiv_orders.setOnClickListener(clickListener)
        tiv_more.setOnClickListener(clickListener)
        iv_language.setOnClickListener(clickListener)
    }

    private fun handleLanguageIcon() {
        if ((application as LaundryApp).language == "en") {
            iv_language.setImageDrawable(resources.getDrawable(R.drawable.ic_english))
        } else if ((application as LaundryApp).language == "es") {
            iv_language.setImageDrawable(resources.getDrawable(R.drawable.ic_spanish))
        }
    }

    private fun handleViewPagerEvents() {
        val mainPagerAdapter = MainPagerAdapter(supportFragmentManager)
        vp_home.adapter = mainPagerAdapter
        vp_home.offscreenPageLimit = 4
    }

    private fun registerBroadcastManagers() {
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(
            onItemAddedToBasket,
            IntentFilter("onItemAddedToBasket")
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (resultCode == Activity.RESULT_OK) {
            val path = ImagePicker.getImagePathFromResult(this, requestCode, resultCode, data)
            setPic(path)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun setPic(mCurrentPhotoPath: String?) {
        (application as LaundryApp).profilePicturePath = mCurrentPhotoPath.toString()
        mCurrentPhotoPath?.let {
            Prefs.getInstance(context = applicationContext)
                .saveString("profilePicturePath", it)
        }
        val intent = Intent("onProfilePictureSelected")
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            2 -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    val builder = StrictMode.VmPolicy.Builder()
                    StrictMode.setVmPolicy(builder.build())
                    ImagePicker.pickImage(this, getString(R.string.select_your_image))
                }
                return
            }
        }
    }


    private val onItemAddedToBasket = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val count = intent.extras?.getInt("count")
            if (count == 0) {
                tv_basket_badge.visibility = View.GONE
            } else {
                tv_basket_badge.text = "" + count
                tv_basket_badge.visibility = View.VISIBLE
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(onItemAddedToBasket)
    }

}
