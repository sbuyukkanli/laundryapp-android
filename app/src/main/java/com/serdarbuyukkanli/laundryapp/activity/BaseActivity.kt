package com.serdarbuyukkanli.laundryapp.activity

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import com.serdarbuyukkanli.laundryapp.BuildConfig
import com.serdarbuyukkanli.laundryapp.LaundryApp
import com.serdarbuyukkanli.laundryapp.R
import com.serdarbuyukkanli.laundryapp.network.RestApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

abstract class BaseActivity : FragmentActivity() {

    protected abstract fun getContext(): Context
    protected abstract fun getLayoutId(): Int
    lateinit var service: RestApi

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setLanguage((application as LaundryApp).language)
        setContentView(getLayoutId())
        service = prepareRetrofit()
    }

    private fun prepareRetrofit(): RestApi {
        val httpClient = OkHttpClient.Builder()
        val logging = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG) {
            logging.level = HttpLoggingInterceptor.Level.BODY
        } else {
            logging.level = HttpLoggingInterceptor.Level.NONE
        }
        httpClient.addInterceptor(logging)

        val retrofit = Retrofit.Builder()
            .baseUrl(getString(R.string.base_url))
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()
        return retrofit.create(RestApi::class.java)
    }

    private fun setLanguage(languageKey: String) {
        val locale = Locale(languageKey)
        Locale.setDefault(locale)
        val config = baseContext.resources.configuration
        config.locale = locale
        baseContext.resources.updateConfiguration(
            config,
            baseContext.resources.displayMetrics
        )
    }

}