package com.serdarbuyukkanli.laundryapp.network

import com.serdarbuyukkanli.laundryapp.model.CategoryWrapper
import com.serdarbuyukkanli.laundryapp.model.ItemWrapper
import retrofit2.Call
import retrofit2.http.*

interface RestApi {

    @GET("/api/cat_list")
    fun getCategories(@Query("language_id") languageId: String): Call<CategoryWrapper>

    @GET("/api/items_list")
    fun getItems(@Query("language_id") languageId: String): Call<ItemWrapper>
}
